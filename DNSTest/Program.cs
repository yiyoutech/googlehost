﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using DNSProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DNSTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (DnsServer server = new DnsServer(IPAddress.Any, 10, 10))
            //{
            //    server.QueryReceived += Server_QueryReceived;

            //    server.Start();

            //    Console.WriteLine("Press any key to stop server");
            //    Console.ReadLine();
            //}

            string hostname = Dns.GetHostName();//得到本机名   
                                                //IPHostEntry localhost = Dns.GetHostByName(hostname);//方法已过期，只得到IPv4的地址   
            IPHostEntry localhost = Dns.GetHostEntry(hostname);
            IPAddress localaddr = localhost.AddressList[0];
            Console.WriteLine(localaddr.ToString());
            Console.ReadLine();
        }

        private static void Server_QueryReceived(object sender, QueryReceivedEventArgs e)
        {
            DnsMessage query = e.Query as DnsMessage;

            if (query == null)
                return;

            DnsMessage response = new DnsMessage
            {
                TransactionID = query.TransactionID,
                IsEDnsEnabled = query.IsEDnsEnabled,
                IsQuery = false,
                OperationCode = query.OperationCode,
                IsRecursionDesired = query.IsRecursionDesired,
                IsCheckingDisabled = query.IsCheckingDisabled,
                IsDnsSecOk = query.IsDnsSecOk,
                Questions = new List<DnsQuestion>(query.Questions)
            };
            if (query.IsEDnsEnabled)
            {
                response.EDnsOptions.Version = query.EDnsOptions.Version;
                response.EDnsOptions.UdpPayloadSize = query.EDnsOptions.UdpPayloadSize;
            }

            if ((query.Questions.Count == 1))
            {
                // send query to upstream server
                DnsQuestion question = query.Questions[0];

                List<string> googleDomain = new List<string>();
                googleDomain.Add("google.com");
                googleDomain.Add("googleapis.com");
                googleDomain.Add("googleusercontent.com");
                googleDomain.Add("ggpht.com");
                googleDomain.Add("gstatic.com");
                googleDomain.Add("googlelabs.com");
                googleDomain.Add("chrome.com");
                int googleDomainCount = googleDomain.Count;
                for (int i = 0; i < googleDomainCount; i++)
                    googleDomain.Add(googleDomain[i] + ".hk");

                if (googleDomain.Exists(x => question.Name.EndsWith(x)))
                {
                    response.AnswerRecords.Add(new ARecord(question.Name, 60, IPAddress.Parse("127.0.0.1")));
                    response.ReturnCode = ReturnCode.NoError;
                }
                else
                {
                    DnsClient client = new DnsClient(IPAddress.Parse("202.96.128.166"), 10000);
                    DnsMessage upstreamResponse = client.Resolve(question.Name, question.RecordType, question.RecordClass);

                    // if got an answer, copy it to the message sent to the client
                    if (upstreamResponse != null)
                    {
                        foreach (DnsRecordBase record in (upstreamResponse.AnswerRecords))
                        {
                            response.AnswerRecords.Add(record);
                        }
                        foreach (DnsRecordBase record in (upstreamResponse.AdditionalRecords))
                        {
                            response.AdditionalRecords.Add(record);
                        }
                        response.ReturnCode = ReturnCode.NoError;
                    }
                }
                // set the response
                e.Response = response;
            }
        }
    }
}

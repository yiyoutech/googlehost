﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using GoogleHost;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace GoogleHosts
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// IP地址池
        /// </summary>
        private List<IPAddress> ipAddressPool = new List<IPAddress>();
        /// <summary>
        /// 总共IP数量
        /// </summary>
        private int totalIPAddress = 0;
        /// <summary>
        /// 有效IP地址集合
        /// </summary>
        private List<TestInfo> validIPAddress = new List<TestInfo>();

        private bool isEnableDHCP;
        private List<IPAddress> originDnsServers;

        Thread scanningThread = null;
        Thread dnsServiceThread = null;

        private static readonly Stopwatch stopWatch = new Stopwatch();
        private static Queue<int> ThreadQueue = new Queue<int>();
        private Random rand = new Random();

        Window settingWindow = null;

        #region 控件事件
        public MainWindow()
        {
            InitializeComponent();
            var rAssembly = Assembly.GetExecutingAssembly().GetName();
            var Version = rAssembly.Version;
            lblProgramName.Content = rAssembly.Name + string.Format(" v{0}.{1}.{2}", Version.Major, Version.Minor, Version.Build);
            DisplayStartButton(true);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //Application.Current.Shutdown();
            if (scanningThread != null)
                scanningThread.Abort();
            if (dnsServiceThread != null)
            {
                if (isEnableDHCP)
                    IPProvider.EnableDHCP(Config.NetworkAdapter);
                else
                    IPProvider.SetDNS(Config.NetworkAdapter, originDnsServers.Select(x => x.ToString()).ToArray());
                dnsServiceThread.Abort();
            }
            Environment.Exit(0);
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            if (settingWindow == null)
            {
                settingWindow = new SettingWindow(this.Left + this.Width, this.Top);
                settingWindow.Owner = this;
                settingWindow.ShowInTaskbar = false;
            }

            settingWindow.Show();
        }

        private void Grid_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void DisplayStartButton(bool display)
        {
            this.Dispatcher.Invoke(new Action(delegate
            {
                if (display)
                {
                    btnStart.Content = "Start";
                    this.loading.Visibility = Visibility.Hidden;
                    btnStart.ToolTip = "开始";
                    lblFound.Visibility = Visibility.Hidden;
                    lblFoundNum.Visibility = Visibility.Hidden;
                    lblSplit.Visibility = Visibility.Hidden;
                    lblChecked.Visibility = Visibility.Hidden;
                    lblCheckedNum.Visibility = Visibility.Hidden;
                    lblThread.Visibility = Visibility.Hidden;
                    lblThreadNum.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnStart.Content = "";
                    this.loading.Visibility = Visibility.Visible;
                    btnStart.ToolTip = "取消";
                    lblFound.Visibility = Visibility.Visible;
                    lblFoundNum.Visibility = Visibility.Visible;
                    lblSplit.Visibility = Visibility.Visible;
                    lblChecked.Visibility = Visibility.Visible;
                    lblCheckedNum.Visibility = Visibility.Visible;
                    lblThread.Visibility = Visibility.Visible;
                    lblThreadNum.Visibility = Visibility.Visible;
                }
            }));
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (btnStart.Content as string == "")
                {
                    if (scanningThread != null)
                    {
                        scanningThread.Abort();
                        Common.ShowMessage(lblMessage, "操作已取消");
                        DisplayStartButton(true);
                    }
                    if (dnsServiceThread != null)
                    {
                        dnsServiceThread.Abort();
                    }
                }
                else
                {
                    Init();
                    scanningThread = new Thread(() =>
                    {
                        if (Config.ValidIP != null)
                        {
                            ipAddressPool.Add(Config.ValidIP);
                            totalIPAddress = ipAddressPool.Count;
                            Common.ShowMessage(lblMessage, string.Format("正在测试原IP" + Config.ValidIP));
                            TestIPAddress();
                        }
                        if (validIPAddress.Count == 0)
                        {
                            Common.ShowMessage(lblMessage, "解析IP...");
                            LoadIPPools();
                            if (ipAddressPool.Count == 0)
                            {
                                Common.ShowMessage(lblMessage, "未解析出IP，请重试.");
                                DisplayStartButton(true);
                                return;
                            }
                            Common.ShowMessage(lblMessage, string.Format("找到{0}个IP. 正在测试IP的可用性...", ipAddressPool.Count));
                            TestIPAddress();
                        }
                        if (validIPAddress.Count == 0)
                        {
                            Common.ShowMessage(lblMessage, "测试完成, 没有IP可以用，放弃吧！");
                            DisplayStartButton(true);
                            return;
                        }
                        Common.ShowMessage(lblMessage, string.Format("测试完成, 共发现{0}个IP可以访问.", validIPAddress.Count));
                        List<TestInfo> fastIp = validIPAddress.FindAll(x => x.PortTime == validIPAddress.Min(y => y.PortTime));
                        Common.ShowMessage(lblMessage, string.Format("最快的IP {0} 速度{1}ms", fastIp[0].Target.Address, fastIp[0].PortTime));
                        Config.ValidIP = fastIp[0].Target.Address;
                        if (Config.WorkingMode == Mode.Host)
                        {
                            if (UpdateHosts(fastIp[0].Target.Address.ToString()))
                                Common.ShowMessage(lblMessage, "修改Host文件完成.");
                        }
                        else if (Config.WorkingMode == Mode.DNS)
                        {
                            StartDnsService();
                        }
                        DisplayStartButton(true);
                    });
                    scanningThread.Start();
                }
            }
            catch (Exception ex)
            {
                Common.ShowMessage(lblMessage, "未知错误," + ex.Message);
            }
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            if (settingWindow != null)
            {
                settingWindow.Left = this.Left + this.Width + 1;
                settingWindow.Top = this.Top;
            }
        }

        private void Init()
        {
            DisplayStartButton(false);
            ipAddressPool = new List<IPAddress>();
            totalIPAddress = 0;
            validIPAddress.Clear();
            stopWatch.Stop();
            stopWatch.Reset();
            stopWatch.Start();
        }

        #endregion

        #region 载入IP
        private void LoadIPPools()
        {
            var domains = new[] { "google.com" };
            ipAddressPool = IPAddressPool.CreateFromDomains(domains);
            totalIPAddress = ipAddressPool.Count;
        }
        #endregion

        #region 测试IP
        private void TestIPAddress()
        {
            EventWaitHandle mre = new ManualResetEvent(false);
            while (true)
            {
                //Common.ShowMessage(lblThreadNum, ThreadQueue.Count.ToString());
                //while (ThreadQueue.Count >= Config.ThreadCount)
                //{
                //    Thread.Sleep(10);
                //    Common.ShowMessage(lblThreadNum, ThreadQueue.Count.ToString());
                //}                
                int a, b;
                ThreadPool.GetMaxThreads(out a, out b);
                if (a != Config.ThreadCount)
                    ThreadPool.SetMaxThreads(Config.ThreadCount, Config.ThreadCount);
                int c, d;
                ThreadPool.GetAvailableThreads(out c, out d);
                Common.ShowMessage(lblThreadNum, (a - c).ToString());

                if (validIPAddress.Count >= Config.FindCount)
                    break;

                if (ipAddressPool.Count == 0)
                    break;
                else
                {
                    ThreadPool.QueueUserWorkItem(obj =>
                    {
                        //lock (ThreadQueue) { ThreadQueue.Enqueue(0); }
                        //try
                        //{                        
                        IPAddress addr = null;
                        lock (ipAddressPool)
                        {
                            if (ipAddressPool.Count > 0)
                            {
                                addr = ipAddressPool[rand.Next(0, ipAddressPool.Count - 1)];
                                ipAddressPool.Remove(addr);
                            }
                        }
                        if (addr != null)
                            TestProcess(new TestInfo(addr));
                        Common.ShowMessage(lblCheckedNum, (totalIPAddress - ipAddressPool.Count).ToString());
                        mre.Set();
                        //}
                        //catch (Exception ex)
                        //{
                        //}
                        //finally
                        //{
                        //    lock (ThreadQueue) { ThreadQueue.Dequeue(); }
                        //}
                    });
                }
                Thread.Sleep(100);
            }
            mre.WaitOne();
        }
        private TestInfo TestProcess(TestInfo info)
        {
            var failedLoops = 0;
            do
            {
                using (var socket = GetSocket(info))
                {
                    if (TestPortViaSocket(socket, info) && TestHttpViaSocket(socket, info))
                    {
                        if (!validIPAddress.Contains(info))
                            validIPAddress.Add(info);
                        Common.ShowMessage(lblFoundNum, validIPAddress.Count.ToString());
                        break;
                    }
                    else
                        failedLoops++;
                }
                Thread.Sleep(10);
            } while (failedLoops < Config.RetryCount);
            return info;
        }
        private bool TestPortViaSocket(Socket socket, TestInfo info)
        {
            try
            {
                var time = stopWatch.ElapsedMilliseconds;
                if (socket.BeginConnect(info.Target, null, null).AsyncWaitHandle.WaitOne(Config.ConnTimeout)
                    && socket.Connected)
                {
                    info.PortTime = stopWatch.ElapsedMilliseconds - time;
                    info.PortOk = true;
                    info.PortMsg = "_OK ";
                }
                else
                {
                    info.PortOk = false;
                    info.PortMsg = "Timeout";
                    info.HttpMsg = "NN PortInvalid";
                }
            }
            catch (Exception ex)
            {
                info.PortOk = false;
                info.PortMsg = ex.Message;
                info.HttpMsg = "NN PortInvalid";
            }

            return info.PortOk;
        }
        private static readonly Regex RxResult = new Regex(@"^(HTTP/... (\d+).*|Server:\s*(\w.*))$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
        private bool TestHttpViaSocket(Socket socket, TestInfo info)
        {
            try
            {
                using (var nets = new NetworkStream(socket))
                {
                    using (var ssls = new SslStream(nets, false, (sender, cert, chain, sslpe) =>
                    {
                        var str = cert.Subject;
                        var len = str.IndexOf(",", 3) - 3;
                        info.CName = str.Substring(3, len > 0 ? len : str.Length - 3);
                        return true;
                    }))
                    {
                        ssls.AuthenticateAsClient(string.Empty);
                        if (ssls.IsAuthenticated)
                        {
                            var data = Encoding.UTF8.GetBytes("HEAD /search?q=g HTTP/1.1\r\nHost: www.google.com.hk\r\n\r\nGET / HTTP/1.1\r\nConnection: close\r\n\r\n");

                            ssls.Write(data);
                            ssls.Flush();

                            using (var sr = new StreamReader(ssls))
                            {
                                var text = sr.ReadToEnd();

                                if (text.Length == 0)
                                {
                                    info.HttpOk = false;
                                    info.HttpMsg = "NN BadResponse";
                                }
                                else
                                {
                                    info.HttpMsg = "NN";

                                    var ms = RxResult.Matches(text);
                                    for (var i = 0; i < ms.Count; i++)
                                    {
                                        if (ms[i].Groups[2].Value == "200" && ++i < ms.Count)
                                            switch (ms[i].Groups[3].Value)
                                            {
                                                case "gws\r":
                                                    info.HttpOk = true;
                                                    info.HttpMsg = "G" + info.HttpMsg[1];
                                                    break;
                                                case "Google Frontend\r":
                                                    info.HttpOk = true;
                                                    info.HttpMsg = info.HttpMsg[0] + "A";
                                                    break;
                                                default:
                                                    i--;
                                                    break;
                                            }
                                    }
                                }
                            }
                        }
                        else
                        {
                            info.HttpOk = false;
                            info.HttpMsg = "NN SslInvalid";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                info.HttpOk = false;
                info.HttpMsg = "NN " + ex.Message;
            }

            return info.HttpOk;
        }
        private Socket GetSocket(TestInfo info, int m = 1)
        {
            var socket = new Socket(info.Target.AddressFamily, SocketType.Stream, ProtocolType.Tcp)
            {
                SendTimeout = Config.ConnTimeout * m,
                ReceiveTimeout = Config.ConnTimeout * m
            };
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, true);

            return socket;
        }
        #endregion

        #region 写入Hosts文件
        private bool UpdateHosts(string ipAddress)
        {
            string hostsPath = @"c:\windows\system32\drivers\etc\hosts";
            string googleHostFlag = "#GoogleHosts";

            StringBuilder sbHosts = new StringBuilder();
            using (StreamReader sr = new StreamReader(hostsPath))
            {
                bool foundGoogleHostContent = false;
                while (!sr.EndOfStream)
                {
                    string hostsLine = sr.ReadLine();
                    //判断是否找到了#GoogleHosts的标识，在这两个标识之间的记录舍弃掉
                    if (hostsLine.ToUpper().Contains(googleHostFlag.ToUpper()))
                    {
                        foundGoogleHostContent = !foundGoogleHostContent;
                    }
                    else
                    {
                        //为False时才读取，在两个标识之间时这个状态是为True的
                        if (!foundGoogleHostContent)
                            sbHosts.AppendLine(hostsLine);
                    }
                }
            }

            List<string> googleServerNames = new List<string>();
            googleServerNames.Add("0-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images1-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("0-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("accounts.google.com");
            googleServerNames.Add("accounts.google.com.hk");
            googleServerNames.Add("myaccount.google.com");
            googleServerNames.Add("myaccount.google.com.hk");
            googleServerNames.Add("www.googleapis.com");
            googleServerNames.Add("ajax.googleapis.com");
            googleServerNames.Add("content.googleapis.com");
            googleServerNames.Add("safebrowsing.google.com");
            //googleServerNames.Add("android.clients.google.com");
            googleServerNames.Add("apis.google.com");
            googleServerNames.Add("android.clients.google.com");
            googleServerNames.Add("books.google.com");
            googleServerNames.Add("books.google.com.hk");
            googleServerNames.Add("calendar.google.com");
            googleServerNames.Add("checkout.google.com");
            googleServerNames.Add("chrome.google.com");
            googleServerNames.Add("clients0.google.com");
            googleServerNames.Add("clients1.google.com");
            googleServerNames.Add("clients1.googleusercontent.com");
            googleServerNames.Add("clients2.google.com");
            googleServerNames.Add("clients2.googleusercontent.com");
            googleServerNames.Add("clients3.google.com");
            googleServerNames.Add("clients3.googleusercontent.com");
            googleServerNames.Add("clients4.google.com");
            googleServerNames.Add("clients4.googleusercontent.com");
            googleServerNames.Add("clients5.google.com");
            googleServerNames.Add("clients6.google.com");
            googleServerNames.Add("code.google.com");
            googleServerNames.Add("contacts.google.com");
            googleServerNames.Add("ditu.google.com");
            googleServerNames.Add("docs.google.com");
            googleServerNames.Add("0.docs.google.com");
            googleServerNames.Add("1.docs.google.com");
            googleServerNames.Add("2.docs.google.com");
            googleServerNames.Add("3.docs.google.com");
            googleServerNames.Add("4.docs.google.com");
            googleServerNames.Add("5.docs.google.com");
            googleServerNames.Add("drive.google.com");
            googleServerNames.Add("drive.google.com.hk");
            googleServerNames.Add("images-pos-opensocial.googleusercontent.com");
            googleServerNames.Add("images1-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images2-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images3-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images4-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images5-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("images6-focus-opensocial.googleusercontent.com");
            googleServerNames.Add("labs.google.com");
            googleServerNames.Add("lh0.ggpht.com");
            googleServerNames.Add("lh1.ggpht.com");
            googleServerNames.Add("lh1.googleusercontent.com");
            googleServerNames.Add("lh2.ggpht.com");
            googleServerNames.Add("lh2.googleusercontent.com");
            googleServerNames.Add("lh3.ggpht.com");
            googleServerNames.Add("lh3.googleusercontent.com");
            googleServerNames.Add("lh4.ggpht.com");
            googleServerNames.Add("lh4.googleusercontent.com");
            googleServerNames.Add("lh5.ggpht.com");
            googleServerNames.Add("lh5.googleusercontent.com");
            googleServerNames.Add("lh6.ggpht.com");
            googleServerNames.Add("lh6.googleusercontent.com");
            googleServerNames.Add("m.google.com");
            googleServerNames.Add("mail.google.com");
            googleServerNames.Add("maps-api-ssl.google.com");
            googleServerNames.Add("news.google.com");
            googleServerNames.Add("news.google.com.hk");
            googleServerNames.Add("oauth.googleusercontent.com");
            googleServerNames.Add("picasaweb.google.com");
            googleServerNames.Add("play.google.com");
            googleServerNames.Add("plus.google.com");
            googleServerNames.Add("pop.gmail.com");
            googleServerNames.Add("profiles.google.com");
            googleServerNames.Add("s1.googleusercontent.com");
            googleServerNames.Add("s2.googleusercontent.com");
            googleServerNames.Add("s3.googleusercontent.com");
            googleServerNames.Add("s4.googleusercontent.com");
            googleServerNames.Add("s5.googleusercontent.com");
            googleServerNames.Add("s6.googleusercontent.com");
            googleServerNames.Add("scholar.l.google.com");
            googleServerNames.Add("services.google.com");
            googleServerNames.Add("security.google.com");
            googleServerNames.Add("spreadsheets.google.com");
            googleServerNames.Add("ssl.gstatic.com");
            googleServerNames.Add("support.google.com");
            googleServerNames.Add("t0.gstatic.com");
            googleServerNames.Add("t1.gstatic.com");
            googleServerNames.Add("t2.gstatic.com");
            googleServerNames.Add("t3.gstatic.com");
            googleServerNames.Add("t4.gstatic.com");
            googleServerNames.Add("t5.gstatic.com");
            googleServerNames.Add("www.gstatic.com");
            googleServerNames.Add("talkgadget.google.com");
            googleServerNames.Add("tools.google.com");
            googleServerNames.Add("translate.google.com");
            googleServerNames.Add("video.google.com");
            googleServerNames.Add("video.google.com.hk");
            googleServerNames.Add("wallet.google.com");
            googleServerNames.Add("webcache.googleusercontent.com");
            googleServerNames.Add("0.client-channel.google.com");
            googleServerNames.Add("1.client-channel.google.com");
            googleServerNames.Add("2.client-channel.google.com");
            googleServerNames.Add("3.client-channel.google.com");
            googleServerNames.Add("4.client-channel.google.com");
            googleServerNames.Add("5.client-channel.google.com");
            googleServerNames.Add("6.client-channel.google.com");
            googleServerNames.Add("7.client-channel.google.com");
            googleServerNames.Add("8.client-channel.google.com");
            googleServerNames.Add("9.client-channel.google.com");
            googleServerNames.Add("10.client-channel.google.com");
            googleServerNames.Add("www.google.com");
            googleServerNames.Add("www.google.com.hk");
            googleServerNames.Add("www.googlelabs.com");
            googleServerNames.Add("www.gstatic.com");
            googleServerNames.Add("chatenabled.mail.google.com");
            googleServerNames.Add("mail-attachment.googleusercontent.com");
            googleServerNames.Add("developers.google.com");
            googleServerNames.Add("developer.chrome.com");
            googleServerNames = googleServerNames.Distinct().ToList<string>();
            googleServerNames.Sort();

            sbHosts.AppendLine(googleHostFlag);
            foreach (string hostName in googleServerNames)
            {
                sbHosts.AppendLine(ipAddress + "\t" + hostName);
            }
            sbHosts.AppendLine(googleHostFlag);
            try
            {
                string tempPath = Path.Combine(System.Environment.GetEnvironmentVariable("TEMP"), @"hosts" + DateTime.Now.Ticks.ToString());
                using (StreamWriter sw = new StreamWriter(tempPath, false))
                {
                    sw.Write(sbHosts);
                    sw.Flush();
                }
                File.Copy(tempPath, hostsPath, true);
                File.Delete(tempPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("写入Hosts文件失败，请以管理员身份执行本程序. " + ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        #endregion

        #region DNS Service
        private void StartDnsService()
        {
            try
            {
                originDnsServers = DnsClient.GetLocalConfiguredDnsServers().Where(x => x.ToString() != "127.0.0.1").ToList();
                isEnableDHCP = IPProvider.IsEnableDHCP(Config.NetworkAdapter);
                List<IPAddress> newDNS = new List<IPAddress>();
                newDNS.Add(IPAddress.Parse("127.0.0.1"));
                newDNS.AddRange(originDnsServers);
                IPProvider.SetDNS(Config.NetworkAdapter, newDNS.Select(x => x.ToString()).ToArray());

                dnsServiceThread = new Thread(() =>
                {
                    using (DnsServer server = new DnsServer(IPAddress.Any, 10, 10))
                    {
                        server.QueryReceived += Server_QueryReceived;
                        server.Start();
                        Common.ShowMessage(lblMessage, "DNS服务已启动");
                        while (true)
                        {
                            Thread.Sleep(1000);
                        }
                    }
                });
                dnsServiceThread.Start();
                //}
                //else
                //{
                //    Common.ShowMessage(lblMessage, "修改本地DNS地址失败!");
                //}
            }
            catch (Exception ex)
            {
                Common.ShowMessage(lblMessage, "未知错误, " + ex.Message);
            }

        }

        private void Server_QueryReceived(object sender, QueryReceivedEventArgs e)
        {
            DnsMessage query = e.Query as DnsMessage;

            if (query == null)
                return;

            DnsMessage response = new DnsMessage
            {
                TransactionID = query.TransactionID,
                IsEDnsEnabled = query.IsEDnsEnabled,
                IsQuery = false,
                OperationCode = query.OperationCode,
                IsRecursionDesired = query.IsRecursionDesired,
                IsCheckingDisabled = query.IsCheckingDisabled,
                IsDnsSecOk = query.IsDnsSecOk,
                Questions = new List<DnsQuestion>(query.Questions)
            };
            if (query.IsEDnsEnabled)
            {
                response.EDnsOptions.Version = query.EDnsOptions.Version;
                response.EDnsOptions.UdpPayloadSize = query.EDnsOptions.UdpPayloadSize;
            }

            if ((query.Questions.Count == 1))
            {
                // send query to upstream server
                DnsQuestion question = query.Questions[0];

                List<string> googleDomain = new List<string>();
                googleDomain.Add("google.com");
                googleDomain.Add("googleapis.com");
                googleDomain.Add("googleusercontent.com");
                googleDomain.Add("ggpht.com");
                googleDomain.Add("gstatic.com");
                googleDomain.Add("googlelabs.com");
                googleDomain.Add("chrome.com");
                int googleDomainCount = googleDomain.Count;
                for (int i = 0; i < googleDomainCount; i++)
                    googleDomain.Add(googleDomain[i] + ".hk");

                if (googleDomain.Exists(x => question.Name.EndsWith(x)))
                {
                    response.AnswerRecords.Add(new ARecord(question.Name, 60, Config.ValidIP));
                    response.ReturnCode = ReturnCode.NoError;
                }
                else
                {
                    DnsClient client = new DnsClient(originDnsServers[0], 10000);
                    DnsMessage upstreamResponse = client.Resolve(question.Name, question.RecordType, question.RecordClass);

                    // if got an answer, copy it to the message sent to the client
                    if (upstreamResponse != null)
                    {
                        foreach (DnsRecordBase record in (upstreamResponse.AnswerRecords))
                        {
                            response.AnswerRecords.Add(record);
                        }
                        foreach (DnsRecordBase record in (upstreamResponse.AdditionalRecords))
                        {
                            response.AdditionalRecords.Add(record);
                        }
                        response.ReturnCode = ReturnCode.NoError;
                    }
                }
                // set the response
                e.Response = response;
            }
        }

        #endregion
    }
}


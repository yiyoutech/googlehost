﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace GoogleHosts
{
    static class Config
    {
        public static int _connTimeout = 3000;
        public static int _ThreadCount = 20;
        public static int _retryCount = 3;
        public static int _findCount = 1;
        public static IPAddress _validIP;
        public static string _networkAdapter;
        public static Mode _mode = Mode.Host;

        static Config()
        {
            using (RegistryKey key = Registry.CurrentUser)
            {
                using (RegistryKey software = key.CreateSubKey("Software\\GoogleHost"))
                {
                    software.Close();
                }
                using (var myReg = key.OpenSubKey("Software\\GoogleHost"))
                {
                    if (myReg.GetValue("ConnTimeout") != null)
                        int.TryParse(myReg.GetValue("ConnTimeout").ToString(), out _connTimeout);
                    if (myReg.GetValue("ThreadCount") != null)
                        int.TryParse(myReg.GetValue("ThreadCount").ToString(), out _ThreadCount);
                    if (myReg.GetValue("RetryCount") != null)
                        int.TryParse(myReg.GetValue("RetryCount").ToString(), out _retryCount);
                    if (myReg.GetValue("FindCount") != null)
                        int.TryParse(myReg.GetValue("FindCount").ToString(), out _findCount);
                    if (myReg.GetValue("ValidIP") != null)
                        IPAddress.TryParse(myReg.GetValue("ValidIP").ToString(), out _validIP);
                    if (myReg.GetValue("NetworkAdapter") != null)
                        _networkAdapter = myReg.GetValue("NetworkAdapter").ToString();
                    if (myReg.GetValue("Mode") != null)
                        Enum.TryParse(myReg.GetValue("Mode").ToString(), out _mode);
                    myReg.Close();
                }
            }
        }

        public static int ConnTimeout
        {
            get { return _connTimeout; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("ConnTimeout", value);
                }
                _connTimeout = value;
            }
        }

        public static int ThreadCount
        {
            get { return _ThreadCount; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("ThreadCount", value);
                }
                _ThreadCount = value;
            }
        }

        public static int RetryCount
        {
            get { return _retryCount; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("RetryCount", value);
                }
                _retryCount = value;
            }
        }

        public static int FindCount
        {
            get { return _findCount; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("FindCount", value);
                }
                _findCount = value;
            }
        }


        public static IPAddress ValidIP
        {
            get { return _validIP; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("ValidIP", value);
                }
                _validIP = value;
            }
        }

        public static string NetworkAdapter
        {
            get { return _networkAdapter; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("NetworkAdapter", value);
                }
                _networkAdapter = value;
            }
        }

        public static Mode WorkingMode
        {
            get { return _mode; }
            set
            {
                using (RegistryKey key = Registry.CurrentUser)
                {
                    RegistryKey software = key.OpenSubKey("Software\\GoogleHost", true); //该项必须已存在
                    software.SetValue("Mode", value);
                }
                _mode = value;
            }
        }
    }
}

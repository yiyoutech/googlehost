﻿using System;
using System.Net.NetworkInformation;
using System.Timers;
using System.Windows;

namespace GoogleHosts
{
    /// <summary>
    /// Interaction logic for SettingWindow.xaml
    /// </summary>
    public partial class SettingWindow : Window
    {
        public SettingWindow(double mainLeft, double mainTop)
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
            this.Left = mainLeft + 1;
            this.Top = mainTop;
            sliderFindCount.Value = Config.FindCount;
            sliderRetryCount.Value = Config.RetryCount;
            sliderThreadCount.Value = Config.ThreadCount;
            sliderTimeout.Value = Config.ConnTimeout / 1000;
            InitNetworkAdapter();
            InitMode();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Config.FindCount = (int)sliderFindCount.Value;
                Config.ThreadCount = (int)sliderThreadCount.Value;
                Config.RetryCount = (int)sliderRetryCount.Value;
                Config.ConnTimeout = (int)sliderTimeout.Value * 1000;
                Config.NetworkAdapter = (string)cmbNetworkAdapter.SelectedItem;
                Config.WorkingMode = (Mode)Enum.Parse(typeof(Mode), cmbMode.SelectedItem.ToString());
                Common.ShowMessageAndAutoHide(lblMsg, "保存成功", 2000);
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存失败！\n" + ex.Message, "提示", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void InitNetworkAdapter()
        {
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                if (adapter.OperationalStatus == OperationalStatus.Up
                    && adapter.NetworkInterfaceType != NetworkInterfaceType.Loopback
                    && adapter.NetworkInterfaceType != NetworkInterfaceType.Tunnel
                    && !adapter.Description.Contains("VMware")
                    && !adapter.Description.Contains("Virtual"))
                {
                    cmbNetworkAdapter.Items.Add(adapter.Description);
                    if (Config.NetworkAdapter == adapter.Description)
                    {
                        cmbNetworkAdapter.SelectedItem = adapter.Description;
                    }
                }
            }
        }

        private void InitMode()
        {
            cmbMode.Items.Add("Host");
            cmbMode.Items.Add("DNS");
            cmbMode.SelectedItem = Config.WorkingMode.ToString();
        }
    }
}

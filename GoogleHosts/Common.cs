﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Controls;

namespace GoogleHosts
{
    public class Common
    {
        /// <summary>
        /// 显示状态信息
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessage(Label label, string msg)
        {
            try
            {
                label.Dispatcher.Invoke(new Action(delegate
                {
                    label.Content = msg;
                }));
            }
            catch
            { }
        }

        /// <summary>
        /// 显示状态信息及自动隐藏
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowMessageAndAutoHide(Label label, string msg, int showTime)
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    label.Dispatcher.Invoke(new Action(delegate
                    {
                        label.Content = msg;
                    }));
                    Thread.Sleep(showTime);
                    label.Dispatcher.Invoke(new Action(delegate
                    {
                        label.Content = string.Empty;
                    }));
                }
                );
                thread.Start();
            }
            catch
            { }
        }
    }
}
